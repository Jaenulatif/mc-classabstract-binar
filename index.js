class BangunDatar{
    constructor(){
        if(this.constructor === BangunDatar){
        throw new Error("This is abstract")
      }
    }
    
    luas(){
        console.log("Luas")
    }
  
    keliling(){
        console.log("Keliling")
    }
  }
  
// class persegi
class Persegi extends BangunDatar{
    
    sisi;
    constructor(sisi){
        super();
        this.sisi = sisi;
    }
    
    luas(){
        console.log("Luas Persegi adalah "+ this.sisi*this.sisi)
    }
  
    keliling(){
        console.log("Keliling Persegi adalah "+this.sisi*4)
    }
  }
  
//class persegi
class PersegiPanjang extends BangunDatar{
    panjang;
    lebar;
    constructor(panjang, lebar){
        super();
        this.panjang = panjang;
        this.lebar = lebar;
    }
    luas(){
        console.log("Luas Persegi Panjang adalah "+ this.panjang * this.lebar)
    }
  
    keliling(){
        console.log("Keliling Persegi Panjang adalah "+ (this.panjang*2 + this.lebar*2))
    }
  }
  
//misal segitiga sama sisi
class Segitiga extends BangunDatar{
    constructor(sisi, tinggi){
        super();
        this.sisi = sisi;
        this.tinggi = tinggi;
    }
    
    luas(){
        console.log("Luas Segitiga adalah "+ (1/2 * this.sisi * this.tinggi))
    }
  
    keliling(){
        console.log("Keliling Segitiga adalah "+ this.sisi * 3 )
    }
}
  
  
  //inisial 
  // let bangunDatar = new BangunDatar()
let persegi1 = new Persegi(5);
persegi1.luas()
persegi1.keliling()
  
let persegiPanjang1 = new PersegiPanjang(4,2);
persegiPanjang1.luas()
persegiPanjang1.keliling()
  
let segitiga1 = new Segitiga(6,5);
segitiga1.luas()
segitiga1.keliling()